Họ tên: Nguyễn Đại Tài
MSSV: 1512470

*Yêu cầu:
Yêu cầu cơ bản (max 9 điểm)

Xây dựng chương trình vẽ 5 loại hình cơ bản:

1. Đường thẳng (line). Dùng hàm MoveToEx và LineTo.

2. Hình chữ nhật (rectangle). Dùng hàm Rectangle. Nếu giữ phím Shift sẽ vẽ hình vuông (Square)

3. Hình Ellipse. Dùng hàm Ellipse. Nếu giữ phím Shift sẽ vẽ hình tròn (Circle)

Cho phép chọn loại hình cần vẽ (từ menu là dễ nhất). Cần thể hiện đang chọn menu nào (Check slide menu để biết cách set thuộc tính CHECKED cho menu item)

Mặc định chưa chỉ định nét vẽ, chưa chỉ định màu vẽ, chưa chỉ định màu tô, loại chổi tô, cứ để mặc định màu đen nét đơn giản là được.

Yêu cầu nâng cao:

1. Bọc tất cả các đối tượng vẽ vào các lớp model. Sử dụng đa xạ (polymorphism) để cài đặt việc quản lý các đối tượng và vẽ hình. Sử dụng mẫu thiết kế prototypes để tạo ra hàng mẫu nhằm vẽ ở chế độ xem trước (preview), sử dụng mẫu thiết kế Factory để tạo mới đối tượng. (1 điểm)

2. Lưu và nạp các object (Lưu mảng các tọa độ ở dạng tập tin nhị phân) - 0.25 điểm.

3. Lưu và nạp hình (dạng bitmap là được, nếu tốt hơn là png). - 0.25 điểm.

*Những gì đã làm được:
	+Xây dựng chương trình vẽ 5 loại hình cơ bản: Line, Rectangle, Square, Ellipse, Circle
	+Cho phép chọn loại hình cần vẽ, mặc định chưa chỉ định nét vẽ, chưa chỉ định màu vẽ, chưa chỉ định màu tô, loại chổi tô

*Đính kèm:
	+Link Demo Youtube(chỉ ai có link mới xem được): https://www.youtube.com/watch?v=dA56GVLErnI&feature=youtu.be
	
	+Link bitbucket đã add tdquang_edu: https://bitbucket.org/sinlong1st/windev/src/3ae1d23313847b38612d158fa82606bdc3b17baf/1512470_Paint/?at=master

