#include "stdafx.h"
#include "Rectangle.h"
///

CRectangle::CRectangle(int _x1, int _y1, int _x2, int _y2, COLORREF _color)
{
	x1 = _x1;
	y1 = _y1;
	x2 = _x2;
	y2 = _y2;
	color = _color;
}


CRectangle::~CRectangle(void)
{

}
void CRectangle::Set(int _x1, int _y1, int _x2, int _y2)
{
	x1 = _x1;
	y1 = _y1;
	x2 = _x2;
	y2 = _y2;
}

void CRectangle::SetX2(int _x2)
{
	x2 = _x2;
}

void CRectangle::Draw(HDC _hdc)
{
	HRGN hRegion = CreateRectRgn(x1, y1, x2, y2);
	HBRUSH hbrush = CreateSolidBrush(color);
	FillRgn(_hdc, hRegion, hbrush);
	DeleteObject(hbrush);
}