﻿// QLChiTieu.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "main.h"
#include <windowsx.h>

#include <CommCtrl.h>

#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#pragma comment(lib, "ComCtl32.lib")


#define MAX_LOADSTRING 100


// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text

TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
					   _In_opt_ HINSTANCE hPrevInstance,
					   _In_ LPTSTR    lpCmdLine,
					   _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);


	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_QLCHITIEU, szWindowClass, MAX_LOADSTRING);

	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}


	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_QLCHITIEU));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{

			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}




//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;


	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_QLCHITIEU));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);

	wcex.hbrBackground	= (HBRUSH)(COLOR_BTNFACE+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_QLCHITIEU);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}


//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and

//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	HWND hWnd;

	hInst = hInstance; // Store instance handle in our global variable


	hWnd = CreateWindow(szWindowClass, L"Quản lý chi tiêu", WS_OVERLAPPEDWINDOW & ~(WS_SIZEBOX),
		CW_USEDEFAULT, 0, DF_WNSIZE_WIDTH, DF_WNSIZE_HEIGHT, NULL, NULL, hInstance, NULL);


	if (!hWnd)

	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return TRUE;

}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu

//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch(message)
	{

		HANDLE_MSG(hWnd, WM_DESTROY, OnDestroy);
		HANDLE_MSG(hWnd, WM_CREATE, OnCreate);
		HANDLE_MSG(hWnd, WM_COMMAND, OnCommand);
		HANDLE_MSG(hWnd, WM_PAINT, OnPaint);
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
		break;
	}

	return 0;
}


// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:

		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;

}

void OnDestroy(HWND hwnd)
{
	g_List->Save(configpath);
	delete g_List;
	DeleteType();
	DeleteRect(arrRect);
	PostQuitMessage(0);
}

BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct)
{
	HFONT titleFont =  CreateFont (35, 0, 0, 0, FW_NORMAL, FALSE, FALSE, FALSE, VIETNAMESE_CHARSET,

		OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, L"Tahoma");

	HFONT grbFont =  CreateFont (15, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, ANSI_CHARSET,
		OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, L"Tahoma");

	HFONT txtFont = grbFont;

	HFONT timeFont =  CreateFont (20, 0, 0, 0, FW_NORMAL, TRUE, FALSE, FALSE, ANSI_CHARSET,
		OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, L"Tahoma");

	HFONT btnFont =  CreateFont (20, 0, 0, 0, FW_NORMAL, FALSE, FALSE, FALSE, ANSI_CHARSET,
		OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, L"Tahoma");

	HFONT totalFont =  CreateFont (30, 0, 0, 0, FW_NORMAL, TRUE, FALSE, FALSE, ANSI_CHARSET,
		OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, L"Tahoma");

	//----------------------------------------------------------------------------------------------------//

	HWND hwnd = CreateWindowEx(0, L"STATIC", L"QUẢN LÝ CHI TIÊU", WS_CHILD | WS_VISIBLE | SS_CENTER, 
		CW_USEDEFAULT, 0, DF_WNSIZE_WIDTH, 60, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, WPARAM(titleFont), TRUE);

	//----------------------------------------------------------------------------------------------------//


	hwnd = CreateWindowEx(0, L"BUTTON", L"Thêm loại chi tiêu", WS_CHILD | WS_VISIBLE | BS_GROUPBOX, 
		20, 150 - 90, DF_WNSIZE_WIDTH - 50, 90, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, WPARAM(grbFont), TRUE);

	hwnd = CreateWindowEx(0, L"STATIC", L"Loại chi tiêu", WS_CHILD | WS_VISIBLE | SS_LEFT, 
		50, 180 - 90, 80, 30, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, WPARAM(txtFont), TRUE);

	cmbType = CreateWindow(WC_COMBOBOX, L"", CBS_DROPDOWNLIST | CBS_HASSTRINGS | WS_CHILD | WS_OVERLAPPED | WS_VISIBLE,
		50, 200 - 90, 120, 30, hWnd, NULL, hInst, NULL);
	SendMessage(cmbType, WM_SETFONT, WPARAM(txtFont), TRUE);
	//----------------------------------------------------------------------------------------------------//

	hwnd = CreateWindowEx(0, L"STATIC", L"Nội dung", WS_CHILD | WS_VISIBLE | SS_LEFT, 
		180, 180 - 90, 80, 30, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, WPARAM(txtFont), TRUE);

	txtContent = CreateWindowEx(0, L"EDIT", L"", WS_CHILD | WS_VISIBLE | SS_LEFT | WS_BORDER, 
		180, 200 - 90, 150, 25, hWnd, NULL, hInst, NULL);
	SendMessage(txtContent, WM_SETFONT, WPARAM(txtFont), TRUE);

	//----------------------------------------------------------------------------------------------------//

	hwnd = CreateWindowEx(0, L"STATIC", L"Số tiền", WS_CHILD | WS_VISIBLE | SS_LEFT, 
		340, 180 - 90, 80, 30, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, WPARAM(txtFont), TRUE);

	txtMoney = CreateWindowEx(0, L"EDIT", L"", WS_CHILD | WS_VISIBLE | SS_CENTER | WS_BORDER | ES_NUMBER, 
		340, 200 - 90, 100, 25, hWnd, NULL, hInst, NULL);
	SendMessage(txtMoney, WM_SETFONT, WPARAM(txtFont), TRUE);

	hwnd = CreateWindowEx(0, L"BUTTON", L"Thêm", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 
		460, 180 - 90, 90, 50, hWnd, (HMENU)BTN_NEW, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, WPARAM(btnFont), TRUE);

	//----------------------------------------------------------------------------------------------------//

	hwnd = CreateWindowEx(0, L"BUTTON", L"Danh sách chi tiêu", WS_CHILD | WS_VISIBLE | BS_GROUPBOX, 
		20, 250 - 90, DF_WNSIZE_WIDTH - 50, 235, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, WPARAM(grbFont), TRUE);

	g_List = new CListView;
	g_List->Create(hWnd, IDC_LISTVIEW, hInst, 375, 150, 50, 280 - 90);




	hwnd = CreateWindowEx(0, L"STATIC", L"Lọc DS", WS_CHILD | WS_VISIBLE | SS_LEFT, 
		440, 290 - 90, 40, 30, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, WPARAM(txtFont), TRUE);

	cmbFillter = CreateWindow(WC_COMBOBOX, L"", CBS_DROPDOWNLIST | CBS_HASSTRINGS | WS_CHILD | WS_OVERLAPPED | WS_VISIBLE,
		440, 310 - 90, 120, 30, hWnd, (HMENU)CMB_FILLTER, hInst, NULL);
	SendMessage(cmbFillter, WM_SETFONT, WPARAM(txtFont), TRUE);

	hwnd = CreateWindowEx(0, L"BUTTON", L"Xóa", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 
		455, 350 - 90, 90, 40, hWnd, (HMENU)BTN_DELETE, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, WPARAM(btnFont), TRUE);

	hwnd = CreateWindowEx(0, L"STATIC", L"Tổng cộng: ", WS_CHILD | WS_VISIBLE | SS_LEFT, 
		200, 450 - 90, 100, 30, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, WPARAM(btnFont), TRUE);

	txtTotal = CreateWindowEx(0, L"STATIC", L"0", WS_CHILD | WS_VISIBLE | SS_LEFT, 
		300, 445 - 90, 200, 30, hWnd, NULL, hInst, NULL);
	SendMessage(txtTotal, WM_SETFONT, WPARAM(totalFont), TRUE);

	//----------------------------------------------------------------------------------------------------//

	hwnd = CreateWindowEx(0, L"BUTTON", L"Thống kê", WS_CHILD | WS_VISIBLE | BS_GROUPBOX, 
		20, 490 - 90, DF_WNSIZE_WIDTH - 50, 220, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, WPARAM(grbFont), TRUE);

	hwnd = CreateWindowEx(0, L"STATIC", L"Ăn uống", WS_CHILD | WS_VISIBLE | SS_LEFT, 
		50, 520 - 90, 80, 30, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, WPARAM(txtFont), TRUE);

	hwnd = CreateWindowEx(0, L"STATIC", L"Di chuyển", WS_CHILD | WS_VISIBLE | SS_LEFT, 
		50, 550 - 90, 80, 30, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, WPARAM(txtFont), TRUE);

	hwnd = CreateWindowEx(0, L"STATIC", L"Nhà cửa", WS_CHILD | WS_VISIBLE | SS_LEFT, 
		50, 580 - 90, 80, 30, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, WPARAM(txtFont), TRUE);
	/**************************************/
	hwnd = CreateWindowEx(0, L"STATIC", L"Xe cộ", WS_CHILD | WS_VISIBLE | SS_LEFT, 
		50, 610 - 90, 80, 30, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, WPARAM(txtFont), TRUE);

	hwnd = CreateWindowEx(0, L"STATIC", L"Nhu yếu phẩm", WS_CHILD | WS_VISIBLE | SS_LEFT, 
		50, 640 - 90, 80, 30, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, WPARAM(txtFont), TRUE);

	hwnd = CreateWindowEx(0, L"STATIC", L"Dịch vụ", WS_CHILD | WS_VISIBLE | SS_LEFT, 
		50, 670 - 90, 80, 30, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, WPARAM(txtFont), TRUE);
	int start = 520 - 90;
	for (int i = 0; i < TYPE_SIZE; ++i)
	{
		txtRatio[i] = CreateWindowEx(0, L"STATIC", L"00 %", WS_CHILD | WS_VISIBLE | SS_LEFT, 
			150, start, 40, 30, hWnd, NULL, hInst, NULL);
		SendMessage(txtRatio[i], WM_SETFONT, WPARAM(txtFont), TRUE);
		start += 30;
	}
	//----------------------------------------------------------------------------------------------------//
	//Set default
	InitType();
	ComboBox_AddString(cmbFillter, L"Tất cả");
	for (int i = 0; i < TYPE_SIZE; ++i)
	{
		ComboBox_AddString(cmbType, g_Type[i]);
		ComboBox_AddString(cmbFillter, g_Type[i]);


	}

	/*for (int i = 1; i <= 12; ++i)
	{
	WCHAR tmp[5];
	_itow(i, tmp, 10);
	ComboBox_AddString(cmbMonth, tmp);
	_itow(i + 2015, tmp, 10);
	ComboBox_AddString(cmbYear, tmp);
	}*/

	//Set default select
	ComboBox_SetCurSel(cmbType, 0);
	/*ComboBox_SetCurSel(cmbYear, 0);
	ComboBox_SetCurSel(cmbMonth, 0);*/
	ComboBox_SetCurSel(cmbFillter, 0);

	//load data

	CreateRect(arrRect);

	TCHAR curpath[MAX_BUFF];
	GetCurrentDirectory(MAX_BUFF, curpath);
	wsprintf(configpath, L"%s\\data.txt", curpath);
	g_List->Load(configpath);
	SetTotal();


	return TRUE;
}

void OnCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
	switch (id)
	{
	case IDM_EXIT:
		DestroyWindow(hwnd);
		break;

	case IDM_ABOUT:
		MessageBox(NULL, _T("Nguyễn Đại Tài, MSSV: 1512470\n"), _T("About"), MB_OK);
		break;

	case BTN_NEW:
		{
			int sizeContent = GetWindowTextLength(txtContent);
			int sizeMoney = GetWindowTextLength(txtMoney);

			if (!sizeContent)
			{
				MessageBox(NULL, L"Chưa nhập nội dung !", L"CẢNH BÁO", MB_ICONWARNING | MB_OK);
				break;
			}

			if (!sizeMoney)
			{
				MessageBox(NULL, L"Chưa nhập số tiền !", L"CẢNH BÁO", MB_ICONWARNING | MB_OK);
				break;
			}
			//chuyen loc ve "Tat ca de co the them moi
			if (ComboBox_GetCurSel(cmbFillter))
			{
				MessageBox(NULL, L"Chuyển lọc danh sách về \"Tất cả\" để thao tác được !", L"CẢNH BÁO", MB_ICONWARNING | MB_OK);
				break;
			}

			WCHAR type[MAX_STRING], content[MAX_STRING], money[MAX_STRING];
			ComboBox_GetLBText(cmbType, ComboBox_GetCurSel(cmbType), type);
			GetWindowText(txtContent, content, MAX_STRING);
			GetWindowText(txtMoney, money, MAX_STRING);

			SetWindowText(txtContent, L"");
			SetWindowText(txtMoney,L"");

			g_List->AddItem(type, content, money);

			//Set total
			SetTotal();
			InvalidateRect(hwnd, NULL, TRUE);
		}
		break;

	case BTN_DELETE:
		if (ComboBox_GetCurSel(cmbFillter))
		{
			MessageBox(NULL, L"Chuyển lọc danh sách về \"Tất cả\" để thao tác được !", L"CẢNH BÁO", MB_ICONWARNING | MB_OK);
			break;
		}
		if (g_List->DeleteCursel())//Xoa thanh cong moi cap nhat total
			SetTotal();
		InvalidateRect(hwnd, NULL, TRUE);
		break;

	case CMB_FILLTER:
		if (codeNotify == CBN_SELCHANGE)
		{
			WCHAR type[MAX_STRING];
			ComboBox_GetLBText(cmbFillter, ComboBox_GetCurSel(cmbFillter), type);
			g_List->Fillter(type);
			SetTotal();
		}
		break;
	}
}

void OnPaint(HWND hWnd)
{
	PAINTSTRUCT ps;
	HDC hdc;

	hdc = BeginPaint(hWnd, &ps);

	for (int i = 0; i < TYPE_SIZE; ++i)
	{
		arrRect[i]->Draw(hdc);
	}

	EndPaint(hWnd, &ps);
}

void FormatNumber(int _num, WCHAR* _buf, int _sizebuff)
{
	std::vector<int> digit;
	int count = 0;
	while (_num)
	{
		++count;
		if (4 == count)
		{
			digit.push_back(-4); //add comma code - 48 
			count = 1;
		}
		digit.push_back(_num % 10);
		_num /= 10;
	}

	if (digit.size() > _sizebuff)
		return;

	char tmp[MAX_STRING];
	int j = 0;
	for (int i = digit.size() - 1; i >= 0; --i, ++j)
		tmp[j] = digit[i] + 48;

	if (!digit.size())
	{
		tmp[j] = '0';
		++j;
	}
	tmp[j] = '\0';

	mbstowcs(_buf, tmp, MAX_STRING);
}

void SetTotal()
{
	WCHAR buff[MAX_STRING];
	int arrRatio[TYPE_SIZE] = {0};
	int total = g_List->GetSum(arrRatio, g_Type);
	FormatNumber(total, buff, MAX_STRING);

	SetWindowText(txtTotal, buff);

	if (!ComboBox_GetCurSel(cmbFillter))
	{
		int sum = 0;
		for (int i = 0 ; i < TYPE_SIZE - 1; ++i) // CHi chay toi type -1 cai con lai lay 100 -
		{
			float tmp = 1.0 * arrRatio[i] / total * 100;
			arrRatio[i] = (int)tmp;
			sum += arrRatio[i];
		}
		arrRatio[TYPE_SIZE - 1] =  100 - sum;
		//Set rectangle draw static
		int x = 240; //diem dau tien
		const int pixel_per_percent = 4;
		for (int i = 0; i < TYPE_SIZE; ++i)
		{
			arrRect[i]->SetX2(x + arrRatio[i] * pixel_per_percent);
			WCHAR buff[MAX_STRING];
			swprintf(buff, L"%d %%", arrRatio[i]);
			SetWindowText(txtRatio[i], buff);
		}
	}

}

void CreateRect(CRectangle**& _arrRect)
{
	int x = 240, y = 425;
	_arrRect = new CRectangle*[TYPE_SIZE];
	for (int i = 0; i < TYPE_SIZE; ++i)
	{
		_arrRect[i] = new CRectangle(x, y, x + 300, y + 20, arrColor[i]);
		y += 30;
	}
}

void DeleteRect(CRectangle**& _arrRect)
{
	for (int i = 0; i < TYPE_SIZE; ++i)
	{
		delete _arrRect[i];
		_arrRect[i] = NULL;
	}
	delete[] _arrRect;
	_arrRect = NULL;
}

void InitType()
{
	g_Type = new WCHAR*[6];
	g_Type[0] = wcsdup(L"Ăn uống"); 
	g_Type[1] = wcsdup(L"Di chuyển");//
	g_Type[2] = wcsdup(L"Nhà cửa");
	g_Type[3] = wcsdup(L"Xe cộ");
	g_Type[4] = wcsdup(L"Nhu yếu phẩm");
	g_Type[5] = wcsdup(L"Dịch vụ");
}

void DeleteType()
{
	for (int i = 0; i < 6; ++i)
	{
		delete[] g_Type[i];
		g_Type[i] = NULL;
	}
	delete[] g_Type;
	g_Type = NULL;
}