﻿#include "stdafx.h"
#include "CListView.h"
#include "resource.h"
#include <shellapi.h>
#include <shlobj.h> //Shell object

#include <shlwapi.h>
#pragma comment(lib, "shlwapi.lib")


CListView::CListView(void)
{
	m_hInst = NULL;
	m_hParent = NULL;
	m_hListView = NULL;
	m_nID = 0;
	m_nItem = 0;
}

CListView::~CListView(void)
{
	for (int i = 0; i < m_item.size(); ++i)
	{
		delete m_item[i];
		m_item[i] = NULL;
	}
	m_item.clear();
	DestroyWindow(m_hListView);
}

void CListView::Create(HWND parentWnd, long ID, HINSTANCE hParentInst, 
					   int nWidth, int nHeight, 
					   int x, int y, long lExtStyle, long lStyle)
{
	InitCommonControls();
	CListView::m_hInst = hParentInst;
	CListView::m_hParent = parentWnd;
	CListView::m_hListView = CreateWindowEx(lExtStyle, WC_LISTVIEW, _T("List View"), 
		WS_CHILD | WS_VISIBLE | WS_VSCROLL | WS_TABSTOP | LVS_REPORT | lStyle,
		x, y, nWidth, nHeight, m_hParent, (HMENU) ID, m_hInst, NULL);
	SendMessage(m_hListView, LVM_SETEXTENDEDLISTVIEWSTYLE, LVS_EX_FULLROWSELECT, LVS_EX_FULLROWSELECT);

	CListView::m_nID = ID;

	//Tạo 3 collumn
	LVCOLUMN lvcType, lvcContent, lvcMoney;

	lvcType.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	lvcType.fmt = LVCFMT_LEFT | LVCF_WIDTH;
	lvcType.cx = (nWidth - 20) / 10 * 3;
	lvcType.pszText = _T("Loại chi tiêu");
	ListView_InsertColumn(m_hListView, 0, &lvcType);

	lvcMoney.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	lvcMoney.fmt = LVCFMT_LEFT | LVCF_WIDTH;
	lvcMoney.cx = (nWidth - 20) / 10 * 3 - 5;
	lvcMoney.pszText = _T("Số tiền");
	ListView_InsertColumn(m_hListView, 2, &lvcMoney);

	lvcContent.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	lvcContent.fmt = LVCFMT_LEFT | LVCF_WIDTH;
	lvcContent.cx = (nWidth - 20) - lvcMoney.cx - lvcType.cx - 3;
	lvcContent.pszText = _T("Nội dung");
	ListView_InsertColumn(m_hListView, 1, &lvcContent);
}



///*****************************************************************************************/
HWND CListView::GetHandle()
{
	return m_hListView;
}

void CListView::AddItem(WCHAR* _type, WCHAR* _content, WCHAR* _money)
{
	CListView::AddString(_type, _content, _money, m_nItem);

	CItem* it = new CItem(_type, _content, _money);
	m_item.push_back(it);

	m_nItem++;
}

void CListView::AddString(WCHAR* _type, WCHAR* _content, WCHAR* _money, int i)
{
	LV_ITEM lv;

	lv.mask = LVIF_TEXT;
	lv.iItem = i;
	lv.iSubItem = 0;
	lv.pszText = _type;

	ListView_InsertItem(m_hListView, &lv);	

	ListView_SetItemText(m_hListView, i, 1, _content);

	ListView_SetItemText(m_hListView, i, 2, _money);
}

void CListView::Selected(int _index)
{
	ListView_SetSelectionMark(m_hListView, _index);
}

BOOL CListView::DeleteCursel()
{
	int index = SendMessage(m_hListView, LVM_GETNEXTITEM, (WPARAM)-1, (LPARAM)LVNI_SELECTED);
	if (0 > index) //not have selected
		return FALSE;
	ListView_DeleteItem(m_hListView, index);
	delete m_item[index];
	m_item[index] = NULL;
	m_item.erase(m_item.begin() + index);
	m_nItem--;
	return TRUE;
}

void CListView::DeleteAll()
{
	ListView_DeleteAllItems(m_hListView);
}

int CListView::GetSum(int* _arrRatio, WCHAR** _g_Type)
{
	int count = ListView_GetItemCount(m_hListView);
	int res = 0;
	for (int i = 0; i < count; ++i)
	{
		WCHAR tmp[MAX_STRING];
		ListView_GetItemText(m_hListView, i, 2, tmp, MAX_STRING);
		int num = _wtoi(tmp);
		res += num;

		ListView_GetItemText(m_hListView, i, 0, tmp,  MAX_STRING);

		if (!wcscmp(tmp, _g_Type[0]))
			_arrRatio[0] += num;
		else if (!wcscmp(tmp, _g_Type[1]))
			_arrRatio[1] += num;
		else if (!wcscmp(tmp, _g_Type[2]))
			_arrRatio[2] += num;
		else if (!wcscmp(tmp, _g_Type[3]))
			_arrRatio[3] += num;
		else if (!wcscmp(tmp, _g_Type[4]))
			_arrRatio[4] += num;
	}
	return res;
}

void CListView::Fillter(WCHAR* _type)
{
	CListView::DeleteAll();
	int add = 0;
	for (int i = 0; i < m_nItem; ++i)
	{
		if (!wcscmp(L"Tất cả", _type) || !wcscmp(_type, m_item[i]->m_type))
		{
			CListView::AddString(m_item[i]->m_type, m_item[i]->m_content, m_item[i]->m_money, add);
			++add;
		}
	}
}

void CListView::Load(TCHAR* _pathfile)
{
	std::wifstream file;
	file.open(_pathfile);
	const std::locale utf16 = std::locale(std::locale(), new std::codecvt_utf16<wchar_t, 0x10ffff, std::consume_header>());
	file.imbue(utf16);
	//get count
	WCHAR tmp[10];
	file.getline(tmp, 10);
	int count = _wtoi(tmp);
	for (int i = 0; i < count; ++i)
	{
		WCHAR bufType[MAX_STRING], bufContent[MAX_STRING], bufMoney[MAX_STRING];
		file.getline(bufType, MAX_STRING);
		file.getline(bufContent, MAX_STRING);
		file.getline(bufMoney, MAX_STRING);
		CListView::AddItem(bufType, bufContent, bufMoney);
	}
	file.close();
}

void CListView::Save(TCHAR* _pathfile)
{
	std::wofstream file;//
	file.open(_pathfile);
	const std::locale utf16 = std::locale(std::locale(), new std::codecvt_utf16<wchar_t, 0x10ffff, std::consume_header>());
	file.imbue(utf16);
	WCHAR count[10];
	_itow(m_nItem, count, 10);
	file.write(count, wcslen(count)); file.write(L"\n", 1);
	for (int i = 0; i < m_nItem; ++i)
	{
		file.write(m_item[i]->m_type, wcslen(m_item[i]->m_type)); file.write(L"\n", 1);
		file.write(m_item[i]->m_content, wcslen(m_item[i]->m_content)); file.write(L"\n", 1);
		file.write(m_item[i]->m_money, wcslen(m_item[i]->m_money)); file.write(L"\n", 1);
	}
	file.close();
}

