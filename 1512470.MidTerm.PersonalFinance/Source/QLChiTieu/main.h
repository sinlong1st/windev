﻿#pragma once

#include "resource.h"
#include "CListView.h"
#include "Rectangle.h"

#include <shlwapi.h>
#pragma comment(lib, "shlwapi.lib")

/*****************************************************************************************/
//Các khai báo hàm
ATOM MyRegisterClass(HINSTANCE);
BOOL InitInstance(HINSTANCE, int);
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

void OnDestroy(HWND hwnd);
BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct);
void OnCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
void OnPaint(HWND hWnd);

void FormatNumber(int _num, WCHAR* _buf, int _sizebuff);
void SetTotal();
void CreateRect(CRectangle**& _arrRect);
void DeleteRect(CRectangle**& _arrRect);
void InitType();
void DeleteType();
/****************************************************************************************/
//Global variables
HWND cmbYear, cmbMonth, cmbType, cmbFillter;
HWND txtContent, txtMoney, txtTotal;
HWND txtRatio[TYPE_SIZE];
CListView* g_List;

const int MAX_BUFF = 256;
TCHAR configpath[MAX_BUFF];

WCHAR** g_Type;

//color
//const COLORREF Red = RGB(255, 0, 0);
//const COLORREF Blue = RGB(0, 0, 255);
//const COLORREF Green = RGB(0, 255, 0);
//const COLORREF Pink = RGB(255, 0, 255);
//const COLORREF Yellow = RGB(255, 255, 0);
//const COLORREF Light_Blue = RGB(0, 255, 255);

COLORREF arrColor[] = {RGB(255, 0, 0), RGB(0, 0, 255), RGB(0, 255, 0), RGB(255, 0, 255), RGB(255, 255, 0), RGB(0, 255, 255)};
CRectangle** arrRect;