#pragma once
class CRectangle
{///
public:
	CRectangle(int _x1, int _y1, int _x2, int _y2, COLORREF _color);
	~CRectangle(void);

	void Set(int _x1, int _y1, int _x2, int _y2);
	void SetX2(int _x2);
	void Draw(HDC _hdc);
private:
	int x1, y1, x2, y2;
	COLORREF color;
};

