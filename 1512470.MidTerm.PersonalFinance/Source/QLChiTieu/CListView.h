﻿#pragma once

#include <vector>
#include <fstream>
#include <locale>
#include <codecvt>

#include <commctrl.h>
#pragma comment(lib, "comctl32.lib")




class CItem
{
public:
	CItem(WCHAR* _type, WCHAR* _content, WCHAR* _money)
	{
		m_type = new WCHAR[wcslen(_type) + 1];
		m_content = new WCHAR[wcslen(_content) + 1];
		m_money = new WCHAR[wcslen(_money) + 1];

		wcscpy(m_type, _type);
		wcscpy(m_content, _content);
		wcscpy(m_money, _money);
	}

	~CItem()
	{
		delete[] m_type;
		delete[] m_content;
		delete[] m_money;
	}

public:
	WCHAR* m_type;
	WCHAR* m_content;
	WCHAR* m_money;
};
class CListView
{
public:
	CListView(void);
	~CListView(void);

	void Create(HWND parentWnd, long ID, HINSTANCE hParentInst, int nWidth, 
		int nHeight, int x = CW_USEDEFAULT, int y = 0,
		long lExtStyle = WS_EX_CLIENTEDGE , 
		long lStyle =LVS_SHOWSELALWAYS);


	HWND	GetHandle();

	void AddItem(WCHAR* _type, WCHAR* _content, WCHAR* _money); 
	void AddString(WCHAR* _type, WCHAR* _content, WCHAR* _money, int i); //add string to list
	void Selected(int _index);
	BOOL DeleteCursel();
	void DeleteAll();
	int GetSum(int* _arrRatio, WCHAR** _g_Type);
	void Fillter(WCHAR* _type);
	void Load(TCHAR* _pathfile);
	void Save(TCHAR* _pathfile);

private:
	HINSTANCE	m_hInst;
	HWND		m_hParent;
	HWND		m_hListView;
	int			m_nID;
	int			m_nItem;
	std::vector<CItem*> m_item;
};

