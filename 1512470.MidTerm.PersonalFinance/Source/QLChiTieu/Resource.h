//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by QLChiTieu.rc
////

#define IDS_APP_TITLE			103

#define IDR_MAINFRAME			128
#define IDD_QLCHITIEU_DIALOG	102
#define IDD_ABOUTBOX			103
#define IDM_ABOUT				104
#define IDM_EXIT				105
#define IDI_QLCHITIEU			107
#define IDI_SMALL				108
#define IDC_QLCHITIEU			109
#define IDC_MYICON				2
#ifndef IDC_STATIC
#define IDC_STATIC				-1

#define IDC_LISTVIEW			110

#define TYPE_SIZE				6
#endif
// Next default values for new objects
//

		

#define DF_WNSIZE_WIDTH		600
#define DF_WNSIZE_HEIGHT	700
#define MAX_STRING			64

#define BTN_NEWMONTH		110
#define BTN_NEW				111
#define BTN_DELETE			112
#define CMB_FILLTER			113


#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS

#define _APS_NO_MFC					130
#define _APS_NEXT_RESOURCE_VALUE	129
#define _APS_NEXT_COMMAND_VALUE		32771
#define _APS_NEXT_CONTROL_VALUE		1000
#define _APS_NEXT_SYMED_VALUE		110
#endif
#endif
