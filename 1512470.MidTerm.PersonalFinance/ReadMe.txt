1. Thông tin cá nhân:
	- Họ tên: Nguyễn Đại Tài	
	- Email: sinlong1st@gmail.com

2. Các chức năng làm được: 
Viết chương trình thống kê chi tiêu hàng ngày:
	- Thêm vào danh sách một mục chi, gồm 3 thành phần: 
		+Loại chi tiêu 
		+Nội dung chi 
		+Số tiền.
	- Xem lại danh sách các mục chi của mình, lưu và nạp vào tập tin text .txt (bài làm của em bị lỗi ko lưu được tiếng việt)
	- Vẽ biểu đồ cột ngang nhằm biểu diễn trực quan tỉ lệ tiêu xài.

3. Các luồng sự kiện chính: 
	- Chương trình nạp dữ liệu từ tập tin vào ListView.
	- Loại chi tiêu mặc định là "Ăn uống", người dùng có thể chọn lại loại chi tiêu khác.
	- Điền vào các ô nội dung và số tiền.
	- Thêm thành công thì trong khung ListView sẽ xuất hiện 1 mục chi tiêu mới tương ứng vừa tạo.
	- Đồng thời với đó, biểu đồ tỷ lệ chi tiêu cũng thay đổi theo để phù hợp với số liệu mới nạp vào.
	- Người dùng nhấn nút "Thoát", chương trình tự lưu thông tin vừa ghi vào tập tin.

4. Các luồng sự kiện phụ:
	- Nếu người dùng điền thông tin không hợp lệ hoặc để trống các ô nội dung hoặc số tiền thì sẽ có thông báo yêu cầu điền đầy đủ. 
5. Link Bitbucket:
	https://bitbucket.org/sinlong1st/windev/src/87fa525f31c67bd3763f73d56b0a9c102fbb2b05/1512470.MidTerm.PersonalFinance/?at=master

6. Link youtube:
	https://youtu.be/8aOjey7jPVw
