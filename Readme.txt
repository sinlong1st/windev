Họ tên: Nguyễn Đại Tài
MSSV: 1512470

* Những gì đã làm được:
-Tạo ra TreeView bên trái
-ListView bên phải. 
 Về Treeview:
-Tạo node root là This PC (My Computer)
-Lấy danh sách các ổ đĩa trong máy và thêm các ổ đĩa vào node root
-Bắt sự kiện Expanding, lấy ra đường dẫn dấu ở PARAM để biết mình phải xư lí thư mục nào, duyệt nội dung thư mục, chỉ lấy các thư mục để thêm vào làm node con.
 Về ListView:
-Hiển thị toàn bộ thư mục và tập tin tương ứng với một đường dẫn
-Bấm đôi vào một thư mục sẽ thấy toàn bộ thư mục con và tập tin.
-Tạo ra ListView có 4 cột: Tên, Loại, Thời gian chỉnh sửa, Dung lượng. Với thư mục chỉ cần set text cho 2 cột Tên và Loại. Với tập tin cần hiển thị tối thiểu là 3 cột: Tên, Thời gian chỉnh sửa, Dung lượng
* Main flow:

1. Chạy chương trình lên, hiển thị node My Computer(This PC) trên TreeView bên trái ở trạng thái collapse (thu gọn). Bấm vào sẽ xổ xuống các node con là danh sách ổ đĩa.

2. Bấm vào ổ đĩa đang ở trạng thái collapse(thu gọn) trong TreeView bên trái sẽ xổ xuống danh sách các thư mục con.

3. Bấm vào 1 file sẽ mở file đó

* Luồng phụ:
- Lỗi file hình bitmap (khi e define xong bấm vào file thì nó báo format file bitmap đó không thể đọc)

*Link bitbucket:
https://bitbucket.org/sinlong1st/windev/src/f3cd07277a10/Source%20Code/?at=master

*Link video Demo:
https://youtu.be/_1wwmV0VHpI