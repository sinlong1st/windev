﻿#pragma once
class CDungLuong
{
private:
	__int64 mTotalSize; //Tổng dung lượng
	__int64 mFreeSpace; //Dung lượng còn trống



public:
	CDungLuong();

	CDungLuong(__int64 totalSize, __int64 freeSpace);


	~CDungLuong();


	static LPWSTR convertByteToStringSize(__int64 nSize);

	LPWSTR getTotalSize();
	LPWSTR getFreeSpace();

};

