#pragma once

#define MATHLIBLIBRARY_API _declspec(dllexport)

#include <windowsX.h>
using namespace std;
#include <vector>
#include <Objbase.h>
#include <objidl.h>
#include <gdiplus.h>
#pragma comment(lib, "Ole32.lib")
#pragma comment (lib,"Gdiplus.lib")
#include <math.h>
using namespace Gdiplus;

namespace MathLibrary
{
	class MATHLIBLIBRARY_API CPaint
	{
	public:
		Point start;
		Point last;
		Pen *pen = new Pen(Color(255, 255, 255, 0), 2);
		virtual void draw(Graphics * grap) = 0;
	};

	class MATHLIBLIBRARY_API CLine :public CPaint
	{
	public:

		void draw(Graphics * grap)
		{
			grap->DrawLine(pen, start, last);
		}
	};

	class MATHLIBLIBRARY_API CRect :public CPaint
	{
	public:

		void draw(Graphics * grap)
		{
			grap->DrawRectangle(pen, start.X, start.Y,
				last.X - start.X, last.Y - start.Y);
		}
	};

	class MATHLIBLIBRARY_API CEllipse : public CPaint
	{
	public:

		void draw(Graphics *grap)
		{
			grap->DrawEllipse(pen, start.X, start.Y,
				last.X - start.X, last.Y - start.Y);
		}

	};

	void MATHLIBLIBRARY_API ChagetoSpecial(Point &start, Point &last)
	{
		if (abs(start.X - last.X) <  abs(start.Y - last.Y))
		{
			if (start.Y < last.Y)
				last.Y = start.Y + abs(start.X - last.X);
			else last.Y = start.Y - abs(start.X - last.X);
		}
		else
		{
			if (start.X < last.X)
				last.X = start.X + abs(start.Y - last.Y);
			else last.X = start.X - abs(start.Y - last.Y);
		}
	}

	class MATHLIBLIBRARY_API CSquare :public CPaint
	{
	public:
		void draw(Graphics *grap)
		{
			ChagetoSpecial(start, last);
			grap->DrawRectangle(pen, start.X, start.Y,
				last.X - start.X, last.Y - start.Y);
		}
	};

	class MATHLIBLIBRARY_API CRound : public CPaint
	{
	public:
		void draw(Graphics *grap)
		{
			ChagetoSpecial(start, last);
			grap->DrawEllipse(pen, start.X, start.Y,
				last.X - start.X, last.Y - start.Y);
		}
	};
}