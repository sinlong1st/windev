// *****************************************************************************
// * This is an automatically generated header file for UI Element definition  *
// * resource symbols and values. Please do not modify manually.               *
// *****************************************************************************

#pragma once

#define Home 2 
#define Home_LabelTitle_RESID 60001
#define Picture 3 
#define Picture_LabelTitle_RESID 60002
#define ID_Ellipse 4 
#define ID_Ellipse_LabelTitle_RESID 60003
#define ID_Ellipse_SmallImages_RESID 527
#define ID_Ellipse_LargeImages_RESID 60004
#define ID_Rectangle 5 
#define ID_Rectangle_LabelTitle_RESID 60005
#define ID_Rectangle_SmallImages_RESID 60006
#define ID_Rectangle_LargeImages_RESID 60007
#define ID_Line 6 
#define ID_Line_LabelTitle_RESID 60008
#define ID_Line_SmallImages_RESID 60009
#define ID_Line_LargeImages_RESID 60010
#define InternalCmd2_LabelTitle_RESID 60011
