//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by 1512470.rc
//

#define IDS_APP_TITLE			103

#define IDR_MAINFRAME			128
#define IDD_1512470WIN04_DIALOG	102
#define IDD_ABOUTBOX			103
#define IDM_ABOUT				104
#define IDM_EXIT				105
#define IDI_1512470WIN04			107
#define IDI_SMALL				108
#define IDC_1512470WIN04			109
#define IDI_ICON1                       110
#define IDR_MENU1                       130
#define ID_FILE_OPEN                    115
#define ID_FILE_SAVE                   117
#define ID_FILE_SAVEAS                    119
#define ID_FILE_NEW                     116
#define ID_HELP_ABOUT                    32799
#define ID_FILE_EXIT                    32798
#define ID_EDIT_UNDO                    32777
#define ID_EDIT_CUT                     32778
#define ID_EDIT_COPY                    32779
#define ID_EDIT_PASTE                   32780
#define ID_EDIT_DELETE                  32781
#define ID_EDIT_VIEW                  32797
#define ID_EDIT_FORMAT                32986
#define IDC_MYICON				2
#ifndef IDC_STATIC
#define IDC_STATIC				-1
#endif
// Next default values for new objects
//
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS

#define _APS_NO_MFC					130
#define _APS_NEXT_RESOURCE_VALUE	129
#define _APS_NEXT_COMMAND_VALUE		32771
#define _APS_NEXT_CONTROL_VALUE		1000
#define _APS_NEXT_SYMED_VALUE		110
#endif
#endif
