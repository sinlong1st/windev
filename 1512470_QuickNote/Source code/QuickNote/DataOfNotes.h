#pragma once
#pragma once
#include "resource.h"



class DataOfNotes
{
private:
	std::wstring _Tag;						
	std::wstring _Content;					
	int _NoteCount;							
public:
	DataOfNotes();
	~DataOfNotes();
	//Ca�c ph��ng th��c set, get
	void setTag(std::wstring Tag)
	{
		_Tag = Tag;
	};
	void setContent(std::wstring Content)
	{
		_Content = Content;
	};

	std::wstring getTag()
	{
		return _Tag;
	}
	std::wstring getContent()
	{
		return _Content;
	};
	
	
	LPWSTR getWindowTag(HWND hWnd) //L��y Tag
	{
		LPWSTR Temp = new WCHAR[10000];
		GetDlgItemTextW(hWnd, AN_TB_TAG, Temp, 10000);
		return Temp;
	}
	LPWSTR getWindowContent(HWND hWnd) //L��y Content
	{
		LPWSTR Temp = new WCHAR[10000];
		GetDlgItemTextW(hWnd, AN_TB_CONTENT, Temp, 10000);
		return Temp;
	}
	
};


