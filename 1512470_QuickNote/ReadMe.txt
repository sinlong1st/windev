Họ và tên: Nguyễn Đại Tài
MSSV: 1512470
Email: sinlong1st@gmail.com

*Những điều em đã làm:
   
+ User press a shortcut key (any key combination as you wish, for example Windows + Space), a dialog will appear. Two textboxes are there: a textbox for typing the note and the other for tags.
+ User types the note, enter multiple tags, seperated by commas and space (“, ”) then click Save. (For example: “test, study, info, todo”)
+ User can browse through all the notes or browse by tags.
+ Do statistics and draw chart based on tags and the number of corresponding notes.
+ Main Functions: setTag(), setContent(), getTag(), getContent(), getListAt(), ReadFile(), writeFile(), AddNote(), ViewNote(), drawPieChart(), ViewStatistics(),...

*Chức năng chính
1. Thêm Tag mới:

_ Kịch bản chính:
   
+ Nhấn tổ hợp phím Ctrl + Space để mở Dialog thêm note
   
+ Nhập tag   
+ Nhập Content
+ Chọn mục TagSugget sau đố nhấn Add Tag nếu muốn sử dụng Tag gợi ý
   
+ Nhấn Save để lưu note lại

_ Kịch bản phụ thứ nhất:
   
+ Để trống mục Tag
   
+ Bấm Save, báo lỗi "Tag must be filled out"

_ Kịch bản phụ thứ hai:
   
+ Để trống mục Content
   
+ Bấm Save, báo lỗi "Content must be filled out"

_ Kịch bản phụ thứ ba: (khi không có dữ liệu trong CSDL nghĩa là sẽ không có tag gợi ý)
   
+ bấm Add Tag
   
+ báo lỗi "There is no recommended tag"

2. View Notes

_ Kịch bản chính:  
+ Bấm chuột phải vô notify icon chọn "View Notes", xuất hiện Dialog View Notes
   
+ Nhấp đúp chuột trái vô một dòng để xem thông tin Tag, xuất hiện Dialog với list view 1 cột là tag 1
 cột là content
   
+ Nhấp đúp chuột trái vô một dòng để xem thông tin chi tiết, xuất hiện Dialog với 2 textbox,1 là Tag
 còn lại là content.

3. View Statistics:

_ Kịch bản chính:  
+ Bấm chuột phải vô notify icon chọn "View Statistics", xuất hiện Dialog View Statistic với biểu đồ tròn và các
 ghi chú tương ứng


*Link theo yêu cầu:
_Link demo youtube: 
	https://youtu.be/Jyat5rup-xI
_Link bitbucket:
    https://bitbucket.org/sinlong1st/windev/src/4099f0753c1158e873006658f678458988f7d758/1512470_QuickNote/?at=master
	
